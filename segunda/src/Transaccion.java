public class Transaccion {
    public static void main(String[] args) {
        Producto producto = new Producto(14.50f, 150, "Stabilo", "Board Premium", "Verde");
        /*producto.setColor("Verde");
        producto.setMarca("Stabilo");
        producto.setModelo("Board Premium");
        producto.setPrecio(14.50f);
        producto.setStock(150);*/
        for (int i = 0; i < 10 && producto.getStock()>0 ; i++) {
            int cantidad = 1+(int)(Math.random()*10);
            if(producto.getStock()>=cantidad) {
                Venta venta = new Venta();
                venta.setCantidad(cantidad);
                venta.setProducto(producto);
                venta.setIgv(0.18f * venta.getProducto().getPrecio() * venta.getCantidad());
                venta.setMontoTotal(venta.getIgv() + venta.getProducto().getPrecio() * venta.getCantidad());

                venta.imprimir();
                producto.setStock(producto.getStock() - venta.getCantidad());
            }
        }
    }
}
